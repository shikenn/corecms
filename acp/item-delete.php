<?php
include ('header.php');
include ('sidebar.php');

?>
    <div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo $custdir; ?>/acp/">Dashboard</a>
            </li>

        </ol>
        <div class="card mb-3">
            <div class="card-header">
                <i class="fad fa-trash"></i> Item delete</div>
            <div class="card-body">
                <?php
                $product_id = stripslashes(mysqli_real_escape_string($mysqliA, $_GET['id']));
                if(empty($product_id))
                {
                    echo '
                            <div class="alert alert-warning" role="alert">
                              <i class="fad fa-exclamation-triangle"></i> Invalid item id (product)!
                            </div>
                         ';
                    header("refresh:3; url=$custdir/acp/store-items.php");
                }
                else
                {
                    //delete
                    $delete = $mysqliA->query("DELETE FROM `store_items` WHERE `id` = '$product_id';") or die (mysqli_error($mysqliA));
                    if($delete === true)
                    {
                        echo '
                            <div class="alert alert-success" role="alert">
                              <i class="fad fa-check-circle"></i> Item was deleted (product)!
                            </div>
                         ';
                        header("refresh:3; url=$custdir/acp/store-items.php");
                    }
                }
                ?>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
<?php
include ('footer.php');
?>