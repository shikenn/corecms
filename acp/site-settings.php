<?php
include ('header.php');
include ('sidebar.php');

?>
    <div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo $custdir; ?>/acp/">Dashboard</a>
            </li>
        </ol>
        <div class="card mb-3">
            <div class="card-header">
                <i class="fad fa-globe-americas"></i> Site Settings</div>
            <div class="card-body">
                <?php
                //papyal thingy
                if(isset($_POST['site_update']))
                {
                    $site_name = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['site_name']));
                    $site_description = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['site_description']));
                    $site_contact = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['site_contact']));
                    $site_discord = stripslashes(mysqli_real_escape_string($mysqliA, $_POST['site_discord']));
                    $site_theme = stripcslashes(mysqli_real_escape_string($mysqliA, $_POST['site_theme']));
                    $site_subdirectory = stripcslashes(mysqli_real_escape_string($mysqliA, $_POST['site_subdirectory']));
                    $site_don = stripcslashes(mysqli_real_escape_string($mysqliA, $_POST['site_don']));
                    //update
                    $paypal_update = $mysqliA->query("UPDATE `site_settings` SET `site_name` = '$site_name', `site_en_don` = '$site_don', `site_description` = '$site_description', `site_contact` = '$site_contact', `site_discord` = '$site_discord', `site_subdirectory` = '$site_subdirectory', `site_theme` = '$site_theme';")  or die (mysqli_error($mysqliA));
                    if($paypal_update === true)
                    {
                        echo '
                            <div class="alert alert-success" role="alert">
                                <i class="fad fa-spinner-third fa-spin"></i> Your settings is updating. Please wait...!
                            </div>
                            ';
                        header("refresh:3; url=$custdir/acp/site-settings.php");
                    }
                    else
                    {
                        echo '
                            <div class="alert alert-warning" role="alert">
                                <i class="fad fa-exclamation-triangle"></i> There\'s been an error! Please try again!<br />If this error continues please contact us on discord!
                            </div>
                                ';
                        header("refresh:5; url=$custdir/acp/site-settings.php");
                    }
                }
                else
                {
                    //let's get data from db
                    $site_query = $mysqliA->query("SELECT * FROM `site_settings`") or die (mysqli_error($mysqliA));
                    while($site_ress = $site_query->fetch_assoc())
                    {
                        $site_name = $site_ress['site_name'];
                        $site_description = $site_ress['site_description'];
                        $site_contact = $site_ress['site_contact'];
                        $site_discord = $site_ress['site_discord'];
                        $site_theme = $site_ress['site_theme'];
                        $site_subdirectory = $site_ress['site_subdirectory'];
                        $site_don = $site_ress['site_en_don'];
                    }
                    ?>
                        <form name="paypal_update" method="post" action="">
                            <div class="form-group">
                                <label for="site_name">Site Name</label>
                                <input type="text" name="site_name" class="form-control" value="<?php echo $site_name; ?>" required>
                                <small>Enter here your desired website title</small>
                            </div>
                            <div class="form-group">
                                <label for="site_description">Website Description</label>
                                <input type="text" name="site_description" class="form-control" value="<?php echo  $site_description; ?>" required>
                                <small>Enter here your desired website descrpiton</small>
                            </div>
                            <div class="form-group">
                                <label for="site_contact">Contact Email address</label>
                                <input type="text" name="site_contact" class="form-control" value="<?php echo  $site_contact; ?>" required>
                                <small>Enter here your contact email address for the website</small>
                            </div>
                            <div class="form-group">
                                <label for="site_discord">Discord Invite Link</label>
                                <input type="text" name="site_discord" class="form-control" value="<?php echo  $site_discord; ?>" required>
                                <small>Enter here your discord invite link!</small>
                            </div>
                            <div class="form-group">
                                <label for="site_theme">Site theme</label>
                                <select name="site_theme" class="form-control">
                                    <option value="<?php echo $site_theme; ?>" selected="selected">Curent Theme - <?php echo strtoupper($site_theme); ?></option>
                                    <option disabled="disabled">-- Themes --</option>
                                    <option value="classic">CLASSIC</option>
                                    <!-- <option value="bfa">BFA</option> disabled -->
                                    <option value="shadowlands">SHADOWLANDS</option>
                                    <option value="legion">LEGION</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="site_don">Donation Status</label>
                                <select name="site_don" class="form-control">
                                    <option value="<?php echo $site_don; ?>" selected="selected">Current Status - <?php if ($site_don == 1) { echo strtoupper('Enabled'); } if ($site_don == 0) {echo strtoupper('Disabled');} ?></option>
                                    <option disabled="disabled">-- Values --</option>
                                    <option value="1">Enable</option>
                                    <option value="0">Disable</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="site_discord">Website Subdiractory</label>
                                <input type="text" name="site_subdirectory" class="form-control" value="<?php echo  $site_subdirectory; ?>">
                                <small>Enter here your Subdirectory looking like this: path/to/website <strong>DO NOT CHANGE IF YOU DON´T MOVE THE WEBSITE FOLDER OR YOU DONT NO WHAT YOU DO (YOU MAY BREAK THINGS)</strong></small>
                            </div>
                            <button type="submit" name="site_update" class="btn btn-primary"><i class="fad fa-check-circle"></i> Update this settings!</button>
                        </form>
                        <?php
                }
                ?>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
<?php
include ('footer.php');
?>
