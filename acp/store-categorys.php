<?php
include ('header.php');
include ('sidebar.php');

?>
    <div id="content-wrapper">

    <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="<?php echo $custdir; ?>/acp/">Dashboard</a>
            </li>

        </ol>
        <div class="card mb-3">
            <div class="card-header">
                <i class="fad fa-list-alt"></i> Store Categorys!</div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover table-dark" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Tools</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Tools</th>
                        </tr>
                        </tfoot>
                        <tbody>
                        <?php
                        $category_query = $mysqliA->query("SELECT * FROM `store_items_categorys`") or die (mysqli_error($mysqliA));
                        $num_query = $category_query->num_rows;
                        if($num_query < 1)
                        {
                            echo '<tr><td colspan="6">There are no Categorys!</td></tr>';
                        }
                        else
                        {
                            while($res = $category_query->fetch_assoc())
                            {
                                $categoryID = $res['id'];
                                $categoryName = $res['name'];

                                echo '
                                    <tr>
                                       <td>'. $categoryID .'</td>
                                       <td>'.$categoryName.'</td>
                                       <td><a href="'.$custdir.'/acp/category-edit.php?id='. $categoryID .'" class="btn btn-sm btn-primary"><i class="fad fa-edit"></i> Edit</a> <a href="'.$custdir.'/acp/category-delete.php?id='. $categoryID .'" class="btn btn-sm btn-danger" onclick="return confirm(\'Are you sure? Please change Item category first, before deleting its category\')"><i class="fad fa-trash"></i> Delete</a></td>
                                    </tr>
                                    ';
                            }
                        }
                        ?>

                        </tbody>
                    </table>
                    <a href="<?php echo $custdir; ?>/acp/category-add.php" class="btn btn-success"><i class="fad fa-plus-circle"></i> Add new category</a>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->
<?php
include ('footer.php');
?>