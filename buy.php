<?php /** @noinspection HtmlUnknownTarget */
include('header.php'); ?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-12">
                <!-- Blog Post -->
                <div class="card mb-4" id="card-wow">
                    <div class="card-body">
                        <h2 class="card-title"><i class="fad fa-coins"></i> Buy coins</h2>
                        <?php
                        if(isset($_SESSION['id']))
                        {
                            if(isset($_POST['pay_now']))
                            {
                                try
                                {
                                    $response = $gateway->purchase(array(
                                        'amount' => $_POST['amount'],
                                        'item_name' => $_POST['item_name'],
                                        'item_number' => $_POST['item_number'],
                                        'currency' => PAYPAL_CURRENCY,
                                        'returnUrl' => PAYPAL_RETURN_URL,
                                        'cancelUrl' => PAYPAL_CANCEL_URL,
                                    ))->send();

                                    if ($response->isRedirect()) {
                                        $response->redirect(); // this will automatically forward the customer
                                    } else {
                                        // not successful
                                        echo $response->getMessage();
                                    }
                                } catch(Exception $e) {
                                    echo $e->getMessage();
                                }
                            }
                            else
                            {

                            }
                        }
                        else
                        {
                            echo '
                                <div class="alert alert-info">
                                    <i class="fad fa-exclamation-circle"></i> You need to be logged in to buy coins!
                                </div>
                            ';
                        }
                        ?>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

<?php include('footer.php'); ?>