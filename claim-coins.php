<?php include('header.php'); ?>

    <!-- Page Content -->
    <div class="container">

        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-12">
                <!-- Blog Post -->
                <div class="card mb-4" id="card-wow">
                    <div class="card-body">
                        <h2 class="card-title"><i class="fad fa-coins"></i> Claim coins</h2>
                        <?php
                        if(isset($_SESSION['id']))
                        {
                            $sessid = $_SESSION['id'];

                            //let's do more stuff
                            $get_acc_id = $mysqliA->query("SELECT `id` FROM `account` WHERE `battlenet_account` = '$sessid';") or die (mysqli_errno($mysqliA));
                            while ($acc = $get_acc_id->fetch_assoc()) {
                                $acc_id = $acc['id'];
                            }

                            $transaction_id = $_GET['id'];
                            if(empty($transaction_id))
                            {
                                echo '
                                <div class="alert alert-info">
                                    <i class="fad fa-exclamation-circle"></i> Invalid Transaction ID
                                </div>
                                ';
                                header('refresh:3; url=/ucp.php');
                            }
                            else
                            {
                                //let's see if was already claimed (you sneaky bastard!)
                                $check_claim = $mysqliA->query("SELECT * FROM `payments` WHERE `id` = '$transaction_id' AND `account_id` = '$acc_id' AND `payment_status` = 'approved' AND `coins_claimed` = '0';") or die (mysqli_error($mysqliA));
                                $num_check = $check_claim->num_rows;
                                if($num_check > 0)
                                {
                                    while($claim_res = $check_claim->fetch_assoc())
                                    {
                                        $amount = $claim_res['amount'];
                                        switch($amount)
                                        {
                                            case "5.00":
                                                $coins = '210';
                                                break;
                                            case "10.00":
                                                $coins = '550';
                                            case "15.00":
                                                $coins = '1100';
                                                break;
                                        }
                                    }
                                    //curent coins
                                    $current_coin_query = $mysqliA->query("SELECT * FROM `account` WHERE `id` = '$acc_id';") or die (mysqli_error($mysqliA));
                                    while($res_acc = $current_coin_query->fetch_assoc())
                                    {
                                        $curent_coins = $res_acc['coins'];
                                    }

                                    //new coins
                                    $new_coins = ($coins+$curent_coins);
                                    //update coins on account
                                    $update_coins = $mysqliA->query("UPDATE `account` SET `coins` = '$new_coins' WHERE `id` = '$acc_id';") or die (mysqli_error($mysqliA));
                                    $update_coins = $mysqliA->query("UPDATE `battlenet_accounts` SET `battlePayCredits` = '$new_coins' WHERE `id` = '$acc_id';") or die (mysqli_error($mysqliA));
                                    if($update_coins === true)
                                    {
                                        //update claim
                                        $update_claim = $mysqliA->query("UPDATE `payments` SET `coins_claimed` = '1' WHERE `id` = '$transaction_id';") or die (mysqli_error($mysqliA));
                                        if($update_claim === true)
                                        {
                                            echo '
                                            <div class="alert alert-success">
                                                <i class="fad fa-exclamation-circle"></i> You  claimed '. $coins .' <span class="badge badge-warning"><i class="fad fa-coin"></i></span> coins!
                                            </div>
                                            ';
                                            header("refresh: 3; url=/ucp.php");
                                        }
                                    }
                                }
                                else
                                {
                                    echo '
                                    <div class="alert alert-info">
                                        <i class="fad fa-exclamation-circle"></i> You already claimed the <span class="badge badge-warning"><i class="fad fa-coin"></i></span> coins for this <strong>order</strong> or <strong>oder</strong> is <span class="text-danger">invalid</span> !
                                    </div>
                                    ';
                                    header('refresh:5; url=/ucp.php');
                                }
                            }
                        }
                        else
                        {
                            echo '
                                <div class="alert alert-info">
                                    <i class="fad fa-exclamation-circle"></i> You need to be logged in to buy coins!<br />
                                    <i class="fad fa-spinner-third fa-spin"></i> Redirecting to login page... 
                                </div>
                            ';
                            header("refresh 3; url=/login.php");
                        }
                        ?>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->

<?php include('footer.php'); ?>